def generate_cats_dict_fixed_value(num_cats, fixed_value="NO"):
    cat_dict = {}
    for i in range(1, num_cats + 1):
        cat_name = f"Cat_{i}"
        cat_dict[cat_name] = fixed_value
    return cat_dict


cats_with_hats = generate_cats_dict_fixed_value(100)

for i in range(1, len(cats_with_hats) + 1):
    for cat, key in enumerate(cats_with_hats.keys(), start=1):
        if cat % i == 0:
            if cats_with_hats[key] == 'NO':
               cats_with_hats[key] ='YES'
            else:
               cats_with_hats[key] = 'NO' 
               
cats_with_hats.items()