def convert_cel_to_far(celsius_degrees):
    F = float(celsius_degrees) * 9/5 + 32 
    return F

def convert_far_to_cel(fahrenheit_degrees):
    C = (float(fahrenheit_degrees) - 32) * 5/9
    return C

F = input("Enter a temparature in degrees in F: ")
print(f"{F} degrees F = {convert_far_to_cel(F): .2f} degrees C")

C = input("Enter a temparature in degrees in C: ")
print(f"{C} degrees F = {convert_cel_to_far(C): .2f} degrees C")
