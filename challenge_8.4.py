number = int(input("Enter a positive integer: "))
for i in range(1, number):
    outcome = number % i
    if outcome == 0:
        print(f"{str(i)} is a factor of {str(number)}") 