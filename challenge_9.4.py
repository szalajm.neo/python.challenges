universities = [
['California Institute of Technology', 2175, 37704],
['Harvard', 19627, 39849],
['Massachusetts Institute of Technology', 10566, 40732],
['Princeton', 7802, 37000],
['Rice', 5879, 35551],
['Stanford', 19535, 40569],
['Yale', 11701, 40500]
]

def enrollment_stats(list_of_lists):
    list_one = []
    list_two = []
    
    for i in list_of_lists:
        list_one.append(i[1])
        list_two.append(i[2])
        
    return_list = [list_one, list_two]
    return return_list

def mean(input_list):
    mean = sum(input_list) / len(input_list)
    return mean

   input_list.sort()
    midpoint = len(input_list) // 2
    
    if len(input_list) % 2:
        return input_list[midpoint]
    else:
        return (input_list[midpoint - 1] + input_list[midpoint]) / 2

print(f"Total students: {sum(enrollment_stats(universities)[0])} \nTotal tutition: {sum(enrollment_stats(universities)[0])}")
print(f"Student mean: {mean(enrollment_stats(universities)[0]): ,.2f}")
print(f"Student median: {median(enrollment_stats(universities)[0]): ,.2f}")
print(f"Tutition mean: $ {mean(enrollment_stats(universities)[1]): ,.2f}")
print(f"Tutition median: $ {median(enrollment_stats(universities)[1]): ,.2f}")



