import pathlib

home = pathlib.Path.home()
practice_files_dir = home / "OneDrive" / "Pulpit" / "python" / "practice_files"
dictonary_images = home / "OneDrive" / "Pulpit" / "python" / "images"
dictonary_images.mkdir()

imgages_list = list(practice_files_dir.glob("**/*image*"))
for path in imgages_list:
    path.replace(dictonary_images / path.name)