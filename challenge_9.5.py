import random

def randon_poetry():
    nouns =  ["fossil", "horse", "aardvark", "judge", "chef", "mango",
    "extrovert", "gorilla"]
    verbs = ["kicks", "jingles", "bounces", "slurps", "meows",
    "explodes", "curdles"]
    adjectives = ["furry", "balding", "incredulous", "fragrant",
    "exuberant", "glistening"]
    prepositions = ["against", "after", "into", "beneath", "upon",
    "for", "in", "like", "over", "within"]
    adverbs = ["curiously", "extravagantly", "tantalizingly",
    "furiously", "sensuously"]


    selected_nouns = random.sample(nouns, 3)
    selected_verbs = random.sample(verbs, 3)
    selected_adjectives = random.sample(adjectives, 3)
    selected_prepositions = random.sample(prepositions, 3)
    selected_adverbs = random.sample(adverbs, 3)
    
    noun_1 = selected_nouns[0]
    noun_2 = selected_nouns[1]
    noun_3 = selected_nouns[2]
    
    verb_1 = selected_verbs[0]
    verb_2 = selected_verbs[1]
    verb_3 = selected_verbs[2]
    
    adjective_1 = selected_adjectives[0]
    adjective_2 = selected_adjectives[1]
    adjective_3 = selected_adjectives[2]
    
    preposition_1 = selected_prepositions[0]
    preposition_2 = selected_prepositions[1]
    
    adverb_1 = selected_adverbs[0]
    adverb_2 = selected_adverbs[1]
    
    vowels = "aeiouy"
    article = ""
    
    if noun_1[0] in vowels:
        article = "An"
    else:
        article = "A"
        
    print(f"{article} {adjective_1} {noun_1}\n{article} {adjective_1} {noun_1} {verb_1} {preposition_1} the {adjective_2} {noun_2}\n{adverb_1}, the {noun_1} {verb_2}\nthe {noun_2} {verb_3} {preposition_2} a {adjective_3} {noun_3}")