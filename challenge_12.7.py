import pandas as pd 

path = "C:/Users/szala/OneDrive/Pulpit/python/practice_files/scores.csv"

df = pd.read_csv(path)
df_high_scores = df.loc[df.groupby('name')['score'].idxmax()]
df_high_scores.rename(columns = {'score': 'high score'}, inplace=True)

output_path = "C:/Users/szala/OneDrive/Pulpit/python/practice_files/high_scores.csv"
df_high_scores.to_csv(output_path, index = False)

