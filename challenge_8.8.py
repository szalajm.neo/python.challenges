import random

number_of_iterations = 10000
sum_of_toss = 0 

for trial in range(number_of_iterations):
    heads_tolly = 0
    tails_tolly = 0
    number_of_toss = 0
    while heads_tolly == 0 or tails_tolly == 0:
        if random.randint(0,1) == 0:
           heads_tolly += 1
        else:
           tails_tolly += 1
        number_of_toss += 1
    sum_of_toss += number_of_toss


ratio = sum_of_toss / number_of_iterations
print(ratio)

