import random

probabality_of_winning_A = 0.87
probabality_of_winning_B = 0.65
probabality_of_winning_C = 0.17

number_of_iterations = 10000
number_of_wins_general = 0 

for trial in range(number_of_iterations):
    number_of_wins_regions = 0
    if random.random () < probabality_of_winning_A:
        number_of_wins_regions +=1
    if random.random () < probabality_of_winning_B:
        number_of_wins_regions += 1
    if random.random () < probabality_of_winning_C:
        number_of_wins_regions += 1

    if number_of_wins_regions >= 2:
        number_of_wins_general += 1
 
print(number_of_wins_general)