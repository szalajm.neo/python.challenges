from PyPDF2 import PdfReader, PdfWriter
import os

class PdfFileSplitter:
    def __init__(self, path):
        self.path = path
        self.writer1 = PdfWriter()
        self.writer2 = PdfWriter()
        
    def split(self, breakpoint):
        try:
            input_pdf = PdfReader(self.path)
        except FileNotFoundError:
            raise FileNotFoundError(f"Plik '{self.path}' nie został znaleziony.")
        if not (0 <= breakpoint <= len(input_pdf.pages)):
            raise ValueError(f"BreakPoint ({breakpoint}) musi być w zakresie 0 - {len(input_pdf.pages)}.")
        for n in range(breakpoint):
            self.writer1.add_page(input_pdf.pages[n])
        for n in range(breakpoint, len(input_pdf.pages)):
            self.writer2.add_page(input_pdf.pages[n])
            
    def write(self):
        base_name = os.path.splitext(os.path.basename(self.path))[0]
        
        output1 = f"{base_name}_1.pdf"
        output2 = f"{base_name}_2.pdf"
        
        with open(output1, "wb") as out1:
            self.writer1.write(out1)
        with open(output2, "wb") as out2:
            self.writer2.write(out2)

        print(f"Plik podzielony: '{output1}' i '{output2}' zapisane pomyślnie.")