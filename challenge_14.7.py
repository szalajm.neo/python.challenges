from PyPDF2 import PdfReader, PdfWriter
from pathlib import Path
import os
import re

def process_pdf(input_pdf):
    def extract_page_number(text):
        match = re.search(r'\b(\d+)\b', text)  # Find a number in the text
        return int(match.group(1)) if match else None

    def normalize_rotation(page):
        rotation = page.rotation or 0  # Get the current rotation in degrees (default to 0 if not set)
        if rotation != 0:
            page.rotate(-rotation)  # Rotate the page back to 0
        return page
DF
    reader = PdfReader(input_pdf)
    pages_with_numbers = []

    for i, page in enumerate(reader.pages):
        text = page.extract_text()
        page_number = extract_page_number(text)
        if page_number is not None:
            normalized_page = normalize_rotation(page)  # Normalize the rotation to 0 degrees
            pages_with_numbers.append((page_number, i, normalized_page))  # (page_number, original_index, page_object)

    sorted_pages = sorted(pages_with_numbers, key=lambda x: x[0])

    writer = PdfWriter()
    for _, _, page in sorted_pages:
        writer.add_page(page)

    output_pdf = os.path.join(os.getcwd(), "formatted_output.pdf")
    with open(output_pdf, 'wb') as output:
        writer.write(output)

    print(f"Formatted PDF saved to: {output_pdf}")
    return output_pdf
