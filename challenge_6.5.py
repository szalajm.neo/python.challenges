def invest(amount, rate, years):
    for n in range(1, years+1):
        amount = amount * (1 + rate)
        print(f"year {n}: ${amount: .2f}")

invest(100, 0.05, 4)