class Animal:
    
   def __init__(self, name, species, breed, birth_year):
        self.name = name
        self.species = species
        self.breed = breed
        self.birth_year = birth_year

   def feeding(self, food):
      return f"{self.name} eats {food}"
    
   def daily_walk_time(self, time_in_minutes):
       return  f"{self.name} needs to spend {time_in_minutes} minutes daily for walk"


class Sheep(Animal):
    def feeding(self, food="grass"):
        return f"{self.name} eats {food}"

    def daily_walk_time(self, time_in_minutes=30):
        return f"{self.name} needs to spend {time_in_minutes} minutes daily for a walk"

dolly = Sheep("Dolly", "Sheep", "Not specified", 2017)
print(dolly.feeding())
print(dolly.daily_walk_time())

class Sheep(Animal):
    def feeding(self, food="grass"):
        return f"{self.name} eats {food}"

    def daily_walk_time(self, time_in_minutes=30):
        return f"{self.name} needs to spend {time_in_minutes} minutes daily for a walk"

dolly = Sheep("Dolly", "Sheep", "Not specified", 2017)
print(dolly.feeding())
print(dolly.daily_walk_time())

class Pig(Animal):
    def feeding(self, food= ["froots", "leaves"]):
        return f"{self.name} eats {food}"

    def daily_walk_time(self, time_in_minutes = 100):
        return f"{self.name} needs to spend {time_in_minutes} minutes daily for a walk"

Piggy = Pig("Piggy", "Pig", "Blondie", 2020)
print(Piggy.feeding())
print(Piggy.daily_walk_time())
